package com.epam.enumeration;

import java.util.Scanner;

public class EnumView {
    enum Menu {
        ONE (1), TWO(2), THREE(3), FOUR(4), FIVE(5), EXIT(6);

        private int key;
        Menu(int key){
            this.key = key;
        }
        public int getKey(){
            return this.key;
        }
    }
    private static Scanner in = new Scanner(System.in);
public static void main(String[] args){
    EnumView view =new EnumView();
    view.displayMenu();
}
    private void outMenu(){
        for(Menu i :Menu.values()){
            switch(i.getKey()){
                case 1:
                    System.out.println("1 -> English");
                    break;
                case 2:
                    System.out.println("2 -> French");
                    break;
                case 3:
                    System.out.println("3 -> Spanish");
                    break;
                case 4:
                    System.out.println("4 -> German");
                    break;
                case 5:
                    System.out.println("5 -> Ukrainian");
                    break;
                case 6:
                    System.out.println("6 -> exit");
                    break;
            }
        }
    }

    public void displayMenu(){
        String key = null;
        do {
            outMenu();
            System.out.println("Choose a language:");
            key = in.nextLine().toUpperCase();
            try{
                System.out.println(getGreeting(Integer.valueOf(key)));
            } catch (Exception e){ }
        }while(!key.equals("6"));
    }
    public static String getGreeting(Integer i){
        switch(i){
            case 1: return "Hello!";
            case 2: return "Bonjour!";
            case 3: return "Hola!";
            case 4: return "Hallo!";
            case 5: return "Привіт!";
            case 6: return "Exit";
            default: return null;
        }
    }
}
