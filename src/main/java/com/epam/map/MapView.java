package com.epam.map;

import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.Map;

public class MapView {
    private  Map<String,String> menu;
    private Map<String,Printable> methods;
    private static Scanner in = new Scanner(System.in);

    public MapView() {
        menu = new LinkedHashMap<>();
        methods = new LinkedHashMap<>();

        menu.put("1", "1 -> English");
        menu.put("2", "2 -> French");
        menu.put("3", "3 -> Spanish");
        menu.put("4", "4 -> German");
        menu.put("5", "5 -> Ukrainian");
        menu.put("X", "X -> exit");

        methods.put("1", this::printEnglish);
        methods.put("2", this::printFrench);
        methods.put("3", this::printSpanish);
        methods.put("4", this::printGerman);
        methods.put("5", this::printUkrainian);
    }
    public static void main(String[] args){
        MapView view = new MapView();
        view.displayMenu();
    }
    private void printEnglish(){
        System.out.println("Hello!!!");
    }
    private void printFrench(){
        System.out.println("Bonjour!!!");
    }
    private void printSpanish(){
        System.out.println("Hola!!!");
    }
    private void printGerman(){
        System.out.println("Hallo!!!");
    }
    private void printUkrainian(){
        System.out.println("Привіт!!!");
    }
    private void getMenu(){
        for(String s :menu.values()){
            System.out.println(s);
        }
    }
    public void displayMenu(){
        String key = null;
        do {
            getMenu();
            System.out.println("Chose a language");
            key = in.nextLine().toUpperCase();
            try {
                methods.get(key).print();
            } catch (Exception e){
                System.out.println("Wrong key!");
            }
        } while(!key.equals("X"));
    }
}
