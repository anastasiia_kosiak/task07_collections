package com.epam.map;

@FunctionalInterface
public interface Printable{
        public void print();
    }
