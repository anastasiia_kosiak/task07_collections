package com.epam.tree;

import java.util.ArrayList;
import java.util.List;

public class BinaryTree<T extends Comparable<T>> {
    private BinaryTree left;
    private BinaryTree right;
    private BinaryTree parent;
    private T value;
    private List<T> list = new ArrayList<>();

    public T getValue(){
        return value;
    }
    public BinaryTree getLeft(){
        return left;
    }
    public BinaryTree getRight(){
        return right;
    }
    public BinaryTree getParent(){
        return parent;
    }
    public BinaryTree(T value, BinaryTree parent){
        this.value = value;
        this.parent = parent;
    }
    public void put(T value){
        if(value.compareTo(this.value)<0) {
            if (this.left == null) {
                this.left = new BinaryTree(value, this);
            } else if (this.left != null) {
                this.left.put(value);
            }
        } else {
                if (this.right == null) {
                    this.right = new BinaryTree(value, this);
                } else if (this.right != null) {
                    this.right.put(value);
                }
            }
        }
    private void pprint(BinaryTree<T>currentNode){
        if(currentNode==null){
            return;
        }
        pprint(currentNode.left);
        list.add(currentNode.value);
        System.out.println(currentNode+ " ");
        if(currentNode.right!=null){
            pprint(currentNode.right);
        }
    }

    public void print(){
        list.clear();
        pprint(this);
        System.out.println();
    }

    @Override
    public String toString() {
        return this.value.toString();
    }

    private BinaryTree<T> psearch(BinaryTree<T> tree, T value){
        if(tree==null){
            return null;
        }
        if(value.compareTo(tree.value)<0){
            return psearch(tree.left,value);
        } else if(value.compareTo(tree.value)>0){
            return psearch(tree.right,value);
        } else if(value.compareTo(tree.value)==0){
            return tree;
        }
        return null;
    }
    public BinaryTree<T> search(T value){
        return psearch(this, value);
    }
    public boolean remove(T value){
        BinaryTree<T> tree = search(value);
        if(tree==null){
            return false;
        }
        BinaryTree<T> currentTree;
        if(tree==this){
            if(tree.right!=null){
                currentTree = tree.right;
            } else {
                currentTree = tree.left;
            }
            while(currentTree.left!=null){
                currentTree = currentTree.left;
            }
            T tmp = currentTree.value;
            this.remove(tmp);
            tree.value = tmp;
            return true;

        }
        if(tree.left==null&&tree.right==null&tree.parent!=null){
            if(tree ==tree.parent.left){
                tree.parent.left=null;
            } else {
                tree.parent.right = null;
            }
            return true;
        }
        if(tree.left!=null &&tree.right==null){
            tree.left.parent = tree.parent;
            if(tree==tree.parent.left){
                tree.parent.left=tree.left;
            } else if(tree==tree.parent.right){
                tree.parent.right =tree.left;
            }
            return true;
        }
        if(tree.left==null&&tree.right!=null){
            tree.right.parent = tree.parent;
            if(tree==tree.parent.left){
                tree.parent.left=tree.right;
            } else if(tree == tree.parent.right){
                tree.parent.right = tree.right;
            }
            return true;
        }
        if(tree.right!=null&&tree.left!=null){
            currentTree = tree.right;
            while (currentTree.left!=null){
                currentTree = currentTree.left;
            }
            if(currentTree.parent ==tree){
                currentTree.left = tree.left;
                tree.left.parent = currentTree;
                if(tree==tree.parent.left){
                    tree.parent.left = currentTree;
                } else if(tree == tree.parent.right){
                    tree.parent.right = currentTree;
                }
                return true;
            } else {
                if(currentTree.right!=null){
                    currentTree.right.parent = currentTree.parent;
                }
                currentTree.parent.left = currentTree.right;
                currentTree.right = tree.right;
                currentTree.left = tree.left;
                tree.left.parent = currentTree;
                tree.right.parent = currentTree;
                if(tree == tree.parent.left){
                    tree.parent.left = currentTree;
                } else if(tree==tree.parent.right){
                    tree.parent.right = currentTree;
                }
                return true;
            }

        }
        return false;
    }
}
