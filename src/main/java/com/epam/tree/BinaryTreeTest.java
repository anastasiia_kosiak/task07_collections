package com.epam.tree;

import java.util.Random;

public class BinaryTreeTest {

    public BinaryTreeTest(){

        BinaryTree<Integer> binaryTree = new BinaryTree<>(10,null);
        binaryTree.put(4);
        binaryTree.put(20);
        binaryTree.put(18);
        binaryTree.put(1);
        binaryTree.put(8);
        System.out.println("Initial tree:");
        binaryTree.print();
        binaryTree.remove(10);
        System.out.println("Removed 10:");
        binaryTree.print();
        System.out.println(binaryTree);
        System.out.println("Left side:");
        System.out.println(binaryTree.getLeft());
        System.out.println("Right side:");
        System.out.println(binaryTree.getRight());

    }
}
