package com.epam;
import com.epam.map.MapView;
import com.epam.map.Printable;
import com.epam.enumeration.EnumView;
import com.epam.tree.BinaryTreeTest;
import sun.security.krb5.internal.APOptions;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Application {
    private Map<String,String> menu;
    private Map<String, Printable> methods;
    private static Scanner in = new Scanner(System.in);

    public Application(){
        menu = new LinkedHashMap<>();
        methods = new LinkedHashMap<>();

        menu.put("1", "1 -> Show Map Menu");
        menu.put("2", "2 -> Show Enum Menu");
        menu.put("3", "3 -> Show Binary Tree");
        menu.put("X", "X -> exit the program");

        methods.put("1", this::showMapView);
        methods.put("2", this::showEnumView);
        methods.put("3", this::showBinaryTree);

    }
    public static void main(String[] args){
        Application app = new Application();
        app.displayMenu();
    }
    private void showMapView(){
        MapView map = new MapView();
        map.displayMenu();
    }
    private void showEnumView(){
        EnumView view = new EnumView();
        view.displayMenu();
    }
    private void showBinaryTree(){
        BinaryTreeTest tree = new BinaryTreeTest();
    }

    private void outMenu(){
        for(String s: menu.values()){
            System.out.println(s);
        }
    }

    public void displayMenu(){
        String key = null;

        do {
            outMenu();
            System.out.println("Choose a menu item:");
            key = in.nextLine().toUpperCase();
            try{
                methods.get(key).print();
            } catch(Exception e) {
                System.out.println("Wrong key value");
            }
        } while(!key.equals("X"));
    }
}
